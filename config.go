package chochat

type Config struct {
	Cockroach struct {
		User     string `envconfig:"default=root:123lapa123lapa"`
		Address  string `envconfig:"default=127.0.0.1"`
		Port     string `envconfig:"default=5432"`
		Database string `envconfig:"default=chochat"`
	}
	Elastic struct {
		Address string `envconfig:"default=http://127.0.0.1:9200"`
	}
	Listen string `envconfig:"default=0.0.0.0:9000"`
	Secret string `envconfig:"default=LK9k8FAJaOTFzKRm960LlCnuj7wVbtwJ"`
}
