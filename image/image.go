package image

import (
	"fmt"
	goimage "image"
	"image/color"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"math"

	"github.com/golang/freetype"
	"golang.org/x/image/tiff"

	"chochat"
)

const ProfilePicSize = 96

var ubuntuFontBytes, _ = ioutil.ReadFile("ubuntu.ttf")
var ubuntuFont, _ = freetype.ParseFont(ubuntuFontBytes)

func NewSymbol(symbol byte) goimage.Image {
	image := goimage.NewNRGBA(goimage.Rect(0, 0, ProfilePicSize, ProfilePicSize))

	a := float64(ProfilePicSize) * 0.5
	asq := a*a - 500
	for y := 0.0; y < ProfilePicSize; y++ {
		for x := 0.0; x < ProfilePicSize; x++ {
			dist := math.Pow(x-a, 2) + math.Pow(y-a, 2) - asq

			var (
				r, g, b, a uint8
			)

			switch {
			case dist < 0: // Simply put the color
				r = 178
				g = 112
				b = 192
				a = 255
			case dist < 255: // Mix the color with the background
				per := dist/255.0

				r = uint8(178 * (1 - per) + 255 * per)
				g = uint8(112 * (1 - per) + 255 * per)
				b = uint8(192 * (1 - per) + 255 * per)
				a = uint8(255 * (1 - per))
			default: // Place the background
				r = 255
				g = 255
				b = 255
				a = 0
			}

			image.Set(int(x), int(y), color.NRGBA{
				R: r,
				G: g,
				B: b,
				A: a,
			})
		}
	}

	// Draw the font
	ctx := freetype.NewContext()
	ctx.SetClip(image.Bounds())
	ctx.SetSrc(goimage.NewUniform(color.RGBA{
		R: 255,
		G: 255,
		B: 255,
		A: 255,
	}))
	ctx.SetFont(ubuntuFont)
	ctx.SetFontSize(64.0)
	ctx.SetDst(image)
	p := freetype.Pt(32, 68)
	_, err := ctx.DrawString(string([]byte{symbol}), p)
	if err != nil {
		panic(err)
	}

	return image
}

func TransformPicture(mime string, data io.Reader) (goimage.Image, error) {
	var img goimage.Image
	var err error

	switch mime {
	case "image/png":
		img, err = png.Decode(data)
		if err != nil {
			return nil, fmt.Errorf("png.Decode(): %s: %w", err, chochat.ErrInvalidImage)
		}
	case "image/jpeg":
		img, err = jpeg.Decode(data)
		if err != nil {
			return nil, fmt.Errorf("jpeg.Decode(): %s: %w", err, chochat.ErrInvalidImage)
		}
	case "image/gif":
		img, err = gif.Decode(data)
		if err != nil {
			return nil, fmt.Errorf("gif.Decode(): %s: %w", err, chochat.ErrInvalidImage)
		}
	case "image/tiff":
		img, err = tiff.Decode(data)
		if err != nil {
			return nil, fmt.Errorf("tiff.Decode(): %s: %w", err, chochat.ErrInvalidImage)
		}
	default:
		return nil, chochat.ErrUnknownMIME
	}

	bounds := img.Bounds()
	clipY := (bounds.Dy() - bounds.Dx()) / 2

	result := goimage.NewNRGBA(goimage.Rect(0, 0, ProfilePicSize, ProfilePicSize))

	a := float64(ProfilePicSize) * 0.5
	asq := a*a - 500
	for y := 0.0; y < ProfilePicSize; y++ {
		for x := 0.0; x < ProfilePicSize; x++ {
			dist := math.Pow(x-a, 2) + math.Pow(y-a, 2) - asq

			var (
				r, g, b, a uint8
			)

			or, og, ob, oa := img.At(
				int(x) * bounds.Dx() / ProfilePicSize,
				int(y) * bounds.Dx() / ProfilePicSize + clipY,
			).RGBA()

			switch {
			case dist < 0: // Simply put the color
				r = uint8(or)
				g = uint8(og)
				b = uint8(ob)
				a = uint8(oa)
			case dist < 255: // Mix the color with the background
				r = uint8(or)
				g = uint8(og)
				b = uint8(ob)
				a = uint8(float64(oa) * (1 - dist/255.0))
			default: // Place the background
				r = 255
				g = 255
				b = 255
				a = 0
			}

			result.Set(int(x), int(y), color.NRGBA{
				R: r,
				G: g,
				B: b,
				A: a,
			})
		}
	}

	return result, nil
}
