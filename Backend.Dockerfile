FROM golang:1.18.3-bullseye

EXPOSE 9000:9000

RUN mkdir -p /root/go/src/chochat
ADD . /root/go/src/chochat
WORKDIR /root/go/src/chochat/cmd/msgserver
RUN go build .
WORKDIR /root/go/src/chochat
CMD ["/root/go/src/chochat/cmd/msgserver/msgserver"]
