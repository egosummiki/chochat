package storage

import (
	"context"
	"database/sql"
	"fmt"
	"math/rand"

	"chochat"
	"chochat/log"

	_ "github.com/lib/pq"
)

type Storage struct {
	db *sql.DB
}

func NewStorage(conf chochat.Config) (*Storage, error) {
	var err error
	storage := &Storage{}

	storage.db, err = sql.Open("postgres",
		fmt.Sprintf("postgres://%s@%s:%s/%s?sslmode=disable",
			conf.Cockroach.User,
			conf.Cockroach.Address,
			conf.Cockroach.Port,
			conf.Cockroach.Database))
	if err != nil {
		return nil, fmt.Errorf("sql.Open(): %w", err)
	}

	return storage, nil
}

func (s *Storage) Close() error {
	if err := s.db.Close(); err != nil {
		return fmt.Errorf("s.db.Close(): %w", err)
	}
	return nil
}

func (s *Storage) GetUserByEmail(ctx context.Context, email string) (chochat.User, error) {
	var user chochat.User

	rows, err := s.db.Query(`SELECT id, email, name, surname, secret FROM users WHERE email = $1`, email)
	if err != nil {
		return user, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Info("could not close rows")
		}
	}()

	if !rows.Next() { // expect only one
		return user, fmt.Errorf("rows.Next()")
	}

	if err := rows.Scan(&user.ID, &user.Email, &user.Name, &user.Surname, &user.Secret); err != nil {
		return user, fmt.Errorf("rows.Scan(): %w", err)
	}

	return user, nil
}

func (s *Storage) CreateUser(ctx context.Context, user chochat.User) error {
	_, err := s.db.Exec(`INSERT INTO users (email, name, surname, secret) VALUES ($1, $2, $3, $4)`,
		user.Email,
		user.Name,
		user.Surname,
		user.Secret)
	if err != nil {
		return fmt.Errorf("s.db.Exec(users): %w", err)
	}

	var userID chochat.UserID
	rows, err := s.db.Query(`SELECT id FROM users WHERE email = $1`, user.Email)
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Info("could not close rows")
		}
	}()

	if !rows.Next() { // expect only one
		return fmt.Errorf("rows.Next()")
	}

	if err := rows.Scan(&userID); err != nil {
		return fmt.Errorf("rows.Scan(): %w", err)
	}

	upid, err := userID.Hash()
	if err != nil {
		return fmt.Errorf("userID.Hash(): %w", err)
	}

	_, err = s.db.Exec(`INSERT INTO initial (upid, symbol) VALUES ($1, $2)`, upid.String(), string(user.Name[0]))
	if err != nil {
		return fmt.Errorf("s.db.Exec(initial): %w", err)
	}
	return nil
}

func (s *Storage) CreateConversation(ctx context.Context, users []chochat.UserPublicID) (chochat.ConvID, error) {
	if len(users) == 0 {
		return 0, fmt.Errorf("no users provided")
	}

	cid := chochat.ConvID(rand.Int() % 2147483646)
	params := []interface{}{cid, users[0].String()}
	query := "INSERT INTO conv (id, upid) VALUES ($1,$2)"
	for i, user := range users[1:] {
		query += fmt.Sprintf(", ($1,$%d)", i+3)
		params = append(params, user.String())
	}

	_, err := s.db.Exec(query, params...)
	if err != nil {
		return 0, fmt.Errorf("s.db.Exec(): %w", err)
	}

	return cid, nil
}

func (s *Storage) CheckIfConversationExists(ctx context.Context, a chochat.UserPublicID, b chochat.UserPublicID) (chochat.ConvID, error) {
	rows, err := s.db.Query(`
		SELECT id FROM conv
		WHERE upid in ($1, $2)
		GROUP BY id
		HAVING count(1) = 2
	`, a.String(), b.String())
	if err != nil {
		return 0, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Info("could not close rows")
		}
	}()

	if !rows.Next() {
		return 0, nil
	}

	var cid int
	err = rows.Scan(&cid)
	if err != nil {
		return 0, fmt.Errorf("rows.Scan(): %w", err)
	}

	return chochat.ConvID(cid), nil
}

func (s *Storage) StoreMessage(ctx context.Context, conv chochat.ConvID, sender chochat.UserPublicID, contents string) error {
	exists, err := s.conversationExists(ctx, conv)
	if err != nil {
		return fmt.Errorf("s.conversationExists(): %w", err)
	}
	if !exists {
		return chochat.ErrConversationNotFound
	}

	_, err = s.db.Exec(`INSERT INTO msg(cid, sender, contents, ts) VALUES ($1, $2, $3, now())`, conv, sender.String(), contents)
	if err != nil {
		return fmt.Errorf("s.db.Exec(): %w", err)
	}
	return nil
}

func (s *Storage) AddContact(ctx context.Context, contact chochat.UserID, owner chochat.UserPublicID) error {
	exists, err := s.contactExists(ctx, contact, owner)
	if err != nil {
		return fmt.Errorf("s.contactExists(): %w", err)
	}
	if exists {
		return chochat.ErrContactAlreadyExists
	}

	_, err = s.db.Exec(`INSERT INTO contact(owner, contact) VALUES ($1, $2)`, owner.String(), contact)
	if err != nil {
		return fmt.Errorf("s.db.Exec(): %w", err)
	}
	return nil
}

func (s *Storage) ListContacts(ctx context.Context, owner chochat.UserPublicID) (contacts []chochat.Contact, err error) {
	rows, err := s.db.Query(`SELECT
								   	u.id, u.name, u.surname
								   FROM contact AS c
								   	LEFT JOIN users AS u ON u.id = c.contact
								   WHERE c.owner = $1`, owner.String())
	if err != nil {
		return nil, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Info("could not close rows")
		}
	}()

	for rows.Next() {
		var (
			id      int
			name    string
			surname string
		)

		if err := rows.Scan(&id, &name, &surname); err != nil {
			return nil, fmt.Errorf("rows.Scan(): %w", err)
		}

		contact, err := chochat.NewContact(chochat.UserID(id), name, surname)
		if err != nil {
			return nil, fmt.Errorf("chochat.NewContact(): %w", err)
		}
		contacts = append(contacts, contact)
	}

	return
}

func (s *Storage) ListConversations(ctx context.Context, owner chochat.UserPublicID) (chochat.ConvMap, error) {
	rows, err := s.db.Query(`
		WITH userConv AS (
			SELECT id 
			FROM conv 
			WHERE upid = $1 GROUP BY id
		)
		SELECT u.id, c.upid
			FROM userConv AS u
				INNER JOIN conv AS c ON c.id = u.id
		WHERE c.upid <> $1`, owner.String())
	if err != nil {
		return nil, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	r := chochat.ConvMap{}

	for rows.Next() {
		var (
			id     chochat.ConvID
			member string
		)
		if err = rows.Scan(&id, &member); err != nil {
			return nil, fmt.Errorf("rows.Scan(): %w", err)
		}
		r[id] = append(r[id], member)
	}

	return r, nil
}

func (s *Storage) ListMessages(ctx context.Context, cid chochat.ConvID, upid string) ([]chochat.Message, error) {
	rows, err := s.db.Query(`SELECT sender, contents, ts FROM msg WHERE cid = $1 ORDER BY ts`, cid)
	if err != nil {
		return nil, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	var r []chochat.Message
	for rows.Next() {
		var msg chochat.Message
		if err = rows.Scan(&msg.Sender, &msg.Contents, &msg.Timestamp); err != nil {
			return nil, fmt.Errorf("rows.Scan(): %w", err)
		}
		if msg.Sender == upid {
			msg.Sender = ""
		}
		r = append(r, msg)
	}

	return r, nil
}

func (s *Storage) GetInitial(ctx context.Context, upid chochat.UserPublicID) (byte, error) {
	rows, err := s.db.Query(`SELECT symbol FROM initial WHERE upid = $1`, upid.String())
	if err != nil {
		return 0, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if !rows.Next() { // expect only one
		return 0, fmt.Errorf("could not find initial")
	}

	var initial string
	if err := rows.Scan(&initial); err != nil {
		return 0, fmt.Errorf("rows.Scan(): %w", err)
	}

	return initial[0], nil
}

func (s *Storage) HasContact(ctx context.Context, userID chochat.UserID, upid chochat.UserPublicID) (bool, error) {
	rows, err := s.db.Query(`SELECT 1 FROM contact WHERE owner=$1 and contact=$2`, upid.String(), userID)
	if err != nil {
		return false, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if !rows.Next() { // expect only one
		return false, nil
	}

	return true, nil
}

func (s *Storage) UploadPicture(ctx context.Context, upid chochat.UserPublicID, picdata []byte) error {
	var err error

	rows, err := s.db.Query(`SELECT 1 FROM picture WHERE upid = $1`, upid.String())
	if err != nil {
		return fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if rows.Next() {
		_, err = s.db.Exec(`UPDATE picture SET picture = $1 WHERE upid = $2`, picdata, upid.String())
	} else {
		_, err = s.db.Exec(`INSERT INTO picture(upid, picture) VALUES ($1, $2)`, upid.String(), picdata)
	}
	if err != nil {
		return fmt.Errorf("s.db.Exec(): %w", err)
	}

	return nil
}

func (s *Storage) GetPicture(ctx context.Context, upid chochat.UserPublicID) ([]byte, error) {
	rows, err := s.db.Query(`SELECT picture FROM picture WHERE upid = $1`, upid.String())
	if err != nil {
		return nil, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if !rows.Next() {
		return nil, chochat.ErrPictureNotFound
	}

	var bytes []byte
	if err = rows.Scan(&bytes); err != nil {
		return nil, fmt.Errorf("could not scan picture")
	}

	return bytes, nil
}

func (s *Storage) contactExists(ctx context.Context, contact chochat.UserID, owner chochat.UserPublicID) (bool, error) {
	rows, err := s.db.Query(`SELECT 1 FROM contact WHERE contact = $1 AND owner = $2`, contact, owner.String())
	if err != nil {
		return false, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if rows.Next() {
		return true, nil
	}

	return false, nil
}

func (s *Storage) conversationExists(ctx context.Context, cid chochat.ConvID) (bool, error) {
	rows, err := s.db.Query(`SELECT 1 FROM conv WHERE id = $1`, cid)
	if err != nil {
		return false, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	if rows.Next() {
		return true, nil
	}

	return false, nil
}

func (s *Storage) ListMembers(ctx context.Context, cid chochat.ConvID) ([]chochat.UserPublicID, error) {
	rows, err := s.db.Query(`SELECT upid FROM conv WHERE id = $1`, cid)
	if err != nil {
		return nil, fmt.Errorf("s.db.Query(): %w", err)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			log.WithError(ctx, err).Error("could not close rows")
		}
	}()

	var result []chochat.UserPublicID

	for rows.Next() {
		var upidStr string
		if err := rows.Scan(&upidStr); err != nil {
			return nil, fmt.Errorf("rows.Scan(): %w", err)
		}

		upid, err := chochat.UPIDFromString(upidStr)
		if err != nil {
			return nil, fmt.Errorf("chochat.UPIDFromString(): %w", err)
		}

		result = append(result, upid)
	}

	return result, nil
}
