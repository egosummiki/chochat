package log

import (
	"context"
	"go.uber.org/zap"
	"golang.org/x/xerrors"
)

type (
	contextLabel string
	Property     struct {
		Name  string
		Value interface{}
	}
)

const loggerLabel contextLabel = "log"

func Prop(name string, value interface{}) Property {
	return Property{
		Name:  name,
		Value: value,
	}
}

func WithError(ctx context.Context, err error, values ...Property) *zap.Logger {
	return With(ctx, append(values, Property{
		Name:  "error",
		Value: err,
	})...)
}

func With(ctx context.Context, values ...Property) *zap.Logger {
	logger := ctx.Value(loggerLabel).(*zap.Logger)

	for _, p := range values {
		switch p.Value.(type) {
		case string:
			logger = logger.With(zap.String(p.Name, p.Value.(string)))
		case int:
			logger = logger.With(zap.Int(p.Name, p.Value.(int)))
		case error:
			logger = logger.With(zap.String(p.Name, p.Value.(error).Error()))
		default:
			panic("invalid log value")
		}
	}

	return logger
}

func Inject(ctx context.Context) (context.Context, error) {
	logger, err := zap.NewProduction()
	if err != nil {
		return ctx, xerrors.Errorf("zap.NewProduction(): %w", err)
	}
	return context.WithValue(ctx, loggerLabel, logger), nil
}
