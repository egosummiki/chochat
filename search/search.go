package search

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	elastic "github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"

	"chochat"
)

type Search struct {
	client *elastic.Client
}

func NewSearch(bind string) (*Search, error) {
	cfg := elastic.Config{
		Addresses: []string{
			bind,
		},
	}
	es, err := elastic.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("elastic.NewDefaultClient(): %w", err)
	}
	return &Search{client: es}, nil
}

func (s *Search) SearchContacts(term string) ([]chochat.Contact, error) {
	var queryBuff bytes.Buffer
	queryMap := map[string]interface{}{
		"query": map[string]interface{}{
			"multi_match": map[string]interface{}{
				"query":  term,
				"fields": []string{"name", "surname"},
			},
		},
	}
	if err := json.NewEncoder(&queryBuff).Encode(queryMap); err != nil {
		return nil, fmt.Errorf("json.Encode(): %w", err)
	}

	res, err := s.client.Search(
		s.client.Search.WithBody(&queryBuff),
	)
	if err != nil {
		return nil, fmt.Errorf("s.client.Search(): %w", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		bodyContents, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, fmt.Errorf("ioutil.ReadAll(): %w", err)
		}
		return nil, fmt.Errorf(string(bodyContents))
	}

	type Result struct {
		Hits struct {
			Hits []struct {
				Index  string `json:"_index"`
				Source struct {
					Name    string `json:"name"`
					Surname string `json:"surname"`
				} `json:"_source"`
			} `json:"hits"`
		} `json:"hits"`
	}
	var hits Result
	if err := json.NewDecoder(res.Body).Decode(&hits); err != nil {
		return nil, fmt.Errorf("json.Decode(): %w", err)
	}

	var contacts []chochat.Contact
	for _, hit := range hits.Hits.Hits {
		contacts = append(contacts, chochat.Contact{
			Name:    hit.Source.Name,
			Surname: hit.Source.Surname,
			UPID:    hit.Index,
		})
	}

	return contacts, nil
}

func (s *Search) IndexContact(contact chochat.Contact) error {
	request := esapi.IndexRequest{
		Index:      contact.UPID,
		DocumentID: contact.UPID,
		Body:       strings.NewReader(fmt.Sprintf("{\"name\": \"%s\", \"surname\": \"%s\"}", contact.Name, contact.Surname)),
		Refresh:    "true",
	}

	res, err := request.Do(context.Background(), s.client)
	if err != nil {
		return fmt.Errorf("request.Do(): %w", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		contents, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return fmt.Errorf("ioutil.ReadAll(): %w", err)
		}
		return fmt.Errorf(string(contents))
	}

	return nil
}
