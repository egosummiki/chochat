package chochat

import (
	"bytes"
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
)

type (
	Secret       string
	AccessToken  string
	UserID       int64
	UserPublicID uuid.UUID
	ConvID       int

	User struct {
		ID      UserID
		Email   string
		Name    string
		Surname string
		Secret  Secret
	}

	Contact struct {
		Name    string `json:"name"`
		Surname string `json:"surname"`
		UPID    string `json:"upid"`
	}

	Message struct {
		Contents  string    `json:"contents"`
		Sender    string    `json:"sender"`
		Timestamp time.Time `json:"timestamp"`
	}

	ConvMap map[ConvID][]string

	Storage interface {
		Close() error

		GetUserByEmail(ctx context.Context, email string) (User, error)
		CreateUser(ctx context.Context, user User) error

		CreateConversation(ctx context.Context, users []UserPublicID) (ConvID, error)
		CheckIfConversationExists(ctx context.Context, a UserPublicID, b UserPublicID) (ConvID, error)
		ListConversations(ctx context.Context, owner UserPublicID) (ConvMap, error)
		ListMembers(ctx context.Context, cid ConvID) ([]UserPublicID, error)

		StoreMessage(ctx context.Context, conv ConvID, sender UserPublicID, contents string) error
		ListMessages(ctx context.Context, cid ConvID, upid string) ([]Message, error)

		AddContact(ctx context.Context, contact UserID, owner UserPublicID) error
		ListContacts(ctx context.Context, owner UserPublicID) (contacts []Contact, err error)
		HasContact(ctx context.Context, userID UserID, upid UserPublicID) (bool, error)

		GetInitial(ctx context.Context, upid UserPublicID) (byte, error)
		UploadPicture(ctx context.Context, upid UserPublicID, picdata []byte) error
		GetPicture(ctx context.Context, upid UserPublicID) ([]byte, error)
	}

	Service interface {
		VerifyUserCredentials(ctx context.Context, email string, password string) (token AccessToken, expires time.Time, err error)
		CreateUser(ctx context.Context, email, name, surname, password string) error
		GetUserPublicID(ctx context.Context, token AccessToken) (UserPublicID, error)
		GetUserProfilePicture(ctx context.Context, upid UserPublicID) ([]byte, error)
		UploadProfilePicture(ctx context.Context, token AccessToken, mime string, data io.Reader) error

		VerifySession(ctx context.Context, token AccessToken) error
		RefreshUserToken(ctx context.Context, token AccessToken) (newToken AccessToken, expires time.Time, err error)
		RevokeToken(ctx context.Context, token AccessToken) error

		CreateConversation(ctx context.Context, token AccessToken, users []UserPublicID) (ConvID, error)
		ListConversations(ctx context.Context, token AccessToken) (ConvMap, error)

		Message(ctx context.Context, token AccessToken, conv ConvID, contents string) error
		ListMessages(ctx context.Context, token AccessToken, cid ConvID) ([]Message, error)

		AddContact(ctx context.Context, token AccessToken, owner UserPublicID) error
		ListContacts(ctx context.Context, token AccessToken) ([]Contact, error)
		SearchContacts(ctx context.Context, token AccessToken, term string) ([]Contact, error)
		HasContact(ctx context.Context, token AccessToken, upid UserPublicID) (bool, error)

		ServeWebSocket(ctx context.Context, writer http.ResponseWriter, request *http.Request, token AccessToken) error
		PostTyping(ctx context.Context, token AccessToken, cid ConvID) error
	}
)

func (userID UserID) Hash() (UserPublicID, error) {
	hashed := sha256.Sum256([]byte(strconv.Itoa(int(userID))))

	userUUID, err := uuid.FromBytes(hashed[16:])
	if err != nil {
		return UserPublicID{}, fmt.Errorf("uuid.FromBytes(): %w", err)
	}
	return UserPublicID(userUUID), nil
}

func (upid UserPublicID) String() string {
	return uuid.UUID(upid).String()
}

func (upid UserPublicID) Equals(other UserPublicID) bool {
	return bytes.Equal(upid[:], other[:])
}

func (cid ConvID) String() string {
	return strconv.FormatInt(int64(cid), 10)
}

func UPIDFromString(s string) (UserPublicID, error) {
	_uuid, err := uuid.Parse(s)
	if err != nil {
		return UserPublicID(_uuid), fmt.Errorf("uuid.Parse(): %w", err)
	}

	return UserPublicID(_uuid), nil
}

func NewContact(user UserID, name, surname string) (Contact, error) {
	upid, err := user.Hash()
	if err != nil {
		return Contact{}, fmt.Errorf("user.Hash(): %w", err)
	}
	return Contact{
		Name:    name,
		Surname: surname,
		UPID:    upid.String(),
	}, nil
}
