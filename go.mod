module chochat

go 1.13

require (
	github.com/elastic/go-elasticsearch/v7 v7.4.1
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/golang/mock v1.3.1
	github.com/google/uuid v1.0.0
	github.com/gorilla/websocket v1.4.1
	github.com/lib/pq v1.2.0
	github.com/ory/fosite v0.30.1
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/vrischmann/envconfig v1.2.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898
)
