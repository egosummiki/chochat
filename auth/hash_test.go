package auth

import (
	"chochat"
	"testing"
)

func TestBCryptHasher_Compare(t *testing.T) {

	bc := BCryptHasher{}

	secret, err := bc.Hash("abcd1234")
	if err != nil {
			t.Errorf("bc.Hash(): %s", err)
	}

	type args struct {
		password string
		secret   chochat.Secret
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Compare#1",
			args: args{
				password: "abcd1234",
				secret:   secret,
			},
			wantErr: false,
		},
		{
			name: "Compare#2 Incorrect",
			args: args{
				password: "1234abcd",
				secret:   secret,
			},
			wantErr: true,
		},
		{
			name: "Compare#2 Bad secret",
			args: args{
				password: "1234abcd",
				secret:   "ugfgdhkfgddfgjhdfg",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := bc.Compare(tt.args.password, tt.args.secret); (err != nil) != tt.wantErr {
				t.Errorf("Compare() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
