package auth

import (
	"chochat"
	"reflect"
	"testing"
)

func TestSignUser(t *testing.T) {
	type args struct {
		secret   chochat.Secret
		userName string
		uid      chochat.UserID
	}
	tests := []struct {
		name string
		args args
		want chochat.AccessToken
	}{
		{
			name: "SignUser#1",
			args: args{
				secret:   "secret",
				userName: "john@gmail.com",
				uid:      707406378,
			},
			want: chochat.AccessToken("am9obkBnbWFpbC5jb207Bi8xGMXV5RKSj44+dkl4voewjqFnpzNXZjE/4+51szU="),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SignUser([]byte(tt.args.secret), tt.args.userName, tt.args.uid); got != tt.want {
				t.Errorf("SignUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerateSecret(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "GenerateSecret#1"},
		{name: "GenerateSecret#2"},
		{name: "GenerateSecret#3"},
		{name: "GenerateSecret#4"},
		{name: "GenerateSecret#5"},
		{name: "GenerateSecret#6"},
		{name: "GenerateSecret#7"},
		{name: "GenerateSecret#8"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GenerateSecret()
			for _, ch := range got {
				if (ch < '0' || ch > '9') && (ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z') {
					t.Errorf("GenerateSecret() = %v", string(got))
				}
			}
		})
	}
}

func TestSplitToken(t *testing.T) {
	type args struct {
		token chochat.AccessToken
	}
	tests := []struct {
		name         string
		args         args
		wantUserName string
		wantSecret   []byte
		wantErr      bool
	}{
		{
			name: "SplitToken",
			args: args{
				token: chochat.AccessToken("c3RvbnVnQHdwLnBsO4NoVraQ1FoLzrXDvoRwP4wpGTyIUEGn1fI015fFwOSO"),
			},
			wantUserName: "stonug@wp.pl",
			wantSecret:   []byte{131, 104, 86, 182, 144, 212, 90, 11, 206, 181, 195, 190, 132, 112, 63, 140, 41, 25, 60, 136, 80, 65, 167, 213, 242, 52, 215, 151, 197, 192, 228, 142},
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotUserName, gotSecret, err := SplitToken(tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("SplitToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotUserName != tt.wantUserName {
				t.Errorf("SplitToken() gotUserName = %v, want %v", gotUserName, tt.wantUserName)
			}
			if !reflect.DeepEqual(gotSecret, tt.wantSecret) {
				t.Errorf("SplitToken() gotSecret = %v, want %v", gotSecret, tt.wantSecret)
			}
		})
	}
}
