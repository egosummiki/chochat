package auth

import (
	"chochat"
	"encoding/base64"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/xerrors"
)

type (
	BCryptHasher struct{}

	Hasher interface {
		Hash(string) (chochat.Secret, error)
		Compare(string, chochat.Secret) error
	}
)

func (_ BCryptHasher) Hash(password string) (chochat.Secret, error) {
	result, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return chochat.Secret(base64.StdEncoding.EncodeToString(result)), nil
}

func (_ BCryptHasher) Compare(password string, secret chochat.Secret) error {
	secretRaw, err := base64.StdEncoding.DecodeString(string(secret))
	if err != nil {
		return xerrors.Errorf("base64.StdEncoding.DecodeString(): %w", err)
	}

	err = bcrypt.CompareHashAndPassword(secretRaw, []byte(password))
	if err != nil {
		return xerrors.Errorf("bcrypt.CompareHashAndPassword(): %w", err)
	}
	return nil
}