package auth

import (
	"chochat"
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"github.com/ory/fosite"
	"github.com/ory/fosite/compose"
	"github.com/ory/fosite/storage"
	rand2 "math/rand"
	"time"
)

type SessionSecret []byte

type Session struct {
	Secret  SessionSecret
	Expires time.Time
}

func NewAuth(conf chochat.Config) (fosite.OAuth2Provider, error) {
	config := &compose.Config{}

	store := storage.NewExampleStore()
	store.Clients = map[string]fosite.Client{
		"chochat": &fosite.DefaultClient{
			ID:            "chochat",
			Secret:        []byte(`$2a$10$IxMdI6d.LIRZPpSfEwNoeu4rY3FhDREsxFJXikcgdRRAStxUlsuEO`), // = "foobar"
			RedirectURIs:  []string{"http://localhost:3000/login", "http://localhost:3000/register", "http://localhost:3000/chat"},
			ResponseTypes: []string{"id_token", "code", "token"},
			GrantTypes:    []string{"implicit", "refresh_token", "authorization_code", "password", "client_credentials"},
			Scopes:        []string{},
		},
	}

	coreStrat := compose.NewOAuth2HMACStrategy(config, []byte("secret"), nil)

	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return nil, fmt.Errorf("rsa.GenerateKey(): %w", err)
	}

	openIDStrat := compose.NewOpenIDConnectStrategy(config, key)

	provider := compose.Compose(
		config,
		store,
		compose.CommonStrategy{
			CoreStrategy:               coreStrat,
			OpenIDConnectTokenStrategy: openIDStrat,
		},
		nil,
		compose.OAuth2AuthorizeExplicitFactory,
		compose.OpenIDConnectExplicitFactory,
		compose.OpenIDConnectHybridFactory,
		compose.OpenIDConnectImplicitFactory,
		compose.OpenIDConnectRefreshFactory,
	)

	return provider, nil
}

func encodeHMAC(uid chochat.UserID, secret SessionSecret) []byte {
	var encodedUID [16]byte
	binary.LittleEndian.PutUint64(encodedUID[:], uint64(uid))
	mac := hmac.New(sha256.New, secret)
	mac.Write(encodedUID[:])
	return mac.Sum(nil)
}

func SignUser(userName string, uid chochat.UserID, secret SessionSecret) chochat.AccessToken {
	result := []byte(userName)
	result = append(result, ';')
	result = append(result, encodeHMAC(uid, secret)...)

	return chochat.AccessToken(base64.StdEncoding.EncodeToString(result))
}

func SplitToken(token chochat.AccessToken) (userName string, hmacSum []byte, err error) {
	var user []byte

	decoded, err := base64.StdEncoding.DecodeString(string(token))
	if err != nil {
		return "", nil, fmt.Errorf("base64.StdEncoding.DecodeString(): %w", err)
	}

	for i, ch := range decoded {
		if ch == ';' {
			hmacSum = decoded[i+1:]
			break
		}
		user = append(user, ch)
	}
	userName = string(user)

	return
}

func CheckSecret(uid chochat.UserID, sessionSecret SessionSecret, mac []byte) bool {
	return hmac.Equal(mac, encodeHMAC(uid, sessionSecret))
}

const secretLen = 24
func GenerateSecret() []byte {
	r := make([]byte, secretLen)
	for i := range r {
		value := rand2.Int() % (26 * 2 + 10)
		if value < 10 {
			r[i] = byte('0' + value)
			continue
		}
		value -= 10
		if value < 26 {
			r[i] = byte('A' + value)
			continue
		}
		value -= 26
		r[i] = byte('a' + value)
	}
	return r
}
