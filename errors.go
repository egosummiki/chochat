package chochat

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	ErrConversationAlreadyExists = fmt.Errorf("conversation already exists")
	ErrPictureNotFound           = fmt.Errorf("picture not found")
	ErrUnknownMIME               = fmt.Errorf("unknown mime type")
	ErrInvalidImage              = fmt.Errorf("invalid image data")
	ErrConversationNotFound      = fmt.Errorf("conversation not found")
	ErrContactAlreadyExists      = fmt.Errorf("contact already exists")

	ErrSessionExpired      = fmt.Errorf("session expired")
	ErrSessionDoesntExists = fmt.Errorf("session does not exists")
	ErrInvalidSession      = fmt.Errorf("invalid session")
)

func IsSessionError(err error) bool {
	return errors.Is(err, ErrInvalidSession) || errors.Is(err, ErrSessionDoesntExists) || errors.Is(err, ErrSessionExpired)
}

func GetStatusCode(err error) int {
	switch {
	case err == nil:
		return http.StatusOK
	case errors.Is(err, ErrSessionDoesntExists):
		return http.StatusForbidden
	case errors.Is(err, ErrUnknownMIME):
		return http.StatusExpectationFailed
	case errors.Is(err, ErrInvalidImage):
		return http.StatusNotAcceptable
	case
		errors.Is(err, ErrInvalidSession),
		errors.Is(err, ErrSessionExpired):
		return http.StatusUnauthorized
	case
		errors.Is(err, ErrContactAlreadyExists),
		errors.Is(err, ErrConversationAlreadyExists):
		return http.StatusAlreadyReported
	case
		errors.Is(err, ErrConversationNotFound),
		errors.Is(err, ErrPictureNotFound):
		return http.StatusNotFound

	}

	return http.StatusInternalServerError
}
