import {LoadToken, ContactEndpoint} from './utils'

export async function fetchContacts() {
    let token = LoadToken();

    const response = await fetch(ContactEndpoint, {
        method: 'get',
        headers: {
            'token': token,
        }
    });

    let resultMap = {};
    const json = await response.json();
    for(let contact in json) {
        resultMap[json[contact].upid] = json[contact];
    }

    return resultMap;
}