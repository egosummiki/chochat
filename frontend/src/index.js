import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/index.css';
import './fonts/lato.css';
import './fonts/nuito.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
