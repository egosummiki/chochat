import React from 'react';
import LoginForm from './components/login'
import NotFound from './components/notfound'
import RegisterForm from './components/register'
import Chat from './components/chat'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import './styles/app.css';
import HomeComponent from "./components/home";
import LogoutComponent from "./components/logout";

export default class App extends React.Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <Switch>
                        <Route path="/login" component={LoginForm}/>
                        <Route path="/register" component={RegisterForm}/>
                        <Route path="/chat" component={Chat}/>
                        <Route path="/logout" component={LogoutComponent}/>
                        <Route path="/" component={HomeComponent}/>
                        <Route path="*" component={NotFound} status={404}/>
                    </Switch>
                </Router>
            </div>
        );
    }
}

