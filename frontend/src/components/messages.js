import React from 'react';
import {checkIfHasContact, fetchMessages, fetchOwnUPID, MessageManager} from "../messages";
import {addYourselfToContacts} from "../search";

function Message(props) {
    let senderClassName = 'message-other';
    if(props.msg.sender === '') {
        senderClassName = 'message-self';
    }
    return (
        <div className="message-row">
            <div className={senderClassName}>
                {props.msg.contents}
            </div>
        </div>
    );
}

export default class Messages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            members: [],
            cid: 0,
            convChange: false,
            datadis: false,
            typing: null,
        };
    }

    async setConversation(cid, members) {
        let messages = await fetchMessages(cid);
        this.setState({
            messages: messages === null ? [] : messages,
            members: members,
            cid: cid,
            convChange: true,
            typing: null,
        });
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.state.convChange) {
            if(this.state.members.length > 0) {
                let dis = await checkIfHasContact(this.state.members[0].upid);
                this.setState({
                    datadis: dis === 'false',
                    convChange: false,
                });
            }
        }

        let chat = document.getElementById('chat-messages');
        chat.scrollTop = chat.scrollHeight;
    }

    setTyping(name) {
        this.setState({
            typing: name,
        });
    }

    appendNewMessage(sender, contents) {
        this.setState({
            messages: this.state.messages.concat({
                sender: sender,
                contents: contents,
            }),
            typing: null,
        });
    }

    render() {
        let dis;
        if(this.state.datadis) {
            dis = (
                <div className="data-disclosure" onClick={(e) => {
                    addYourselfToContacts(this.state.members[0].upid);
                    this.setState((state, props) => ({
                        datadis: false,
                        convChange: false,
                    }))
                }}>Klinij aby udostępnić użytkownikowi swoje dane</div>
            );
        }

        let typingBox;
        if(this.state.typing !== null) {
            typingBox = (
                <div className="typing-box">
                    {this.state.typing} pisze...
                </div>
            );
        }

        return (
            <div className="chat" id="chat-messages">
                {dis}
                {this.state.messages.map((msg) => (<Message msg={msg} />))}
                {typingBox}
            </div>
        );
    }
}
