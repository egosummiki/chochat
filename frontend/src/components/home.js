import React from 'react';
import logo from "../images/logo.png";
import '../styles/home.css';

export default function HomeComponent() {
    return (
        <div className='home-message'>
            <div className="logo">
                <img src={logo} alt="cho-chat"/>
            </div>
            <div className='message'>
                <p>Witamy w aplikacji <b>Chochat</b></p>
                <p>możesz się <a href='/login'>zalogować </a>albo <a href='/register'>założyć konto</a></p>
            </div>
        </div>
    );
}