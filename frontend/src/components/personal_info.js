import * as React from "react";
import {fetchOwnUPID, revokeToken, uploadProfilePicture} from "../messages";
import personPic from '../images/person.png';
import logoutPic from '../images/logout.png';

export default class PersonalInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            upid: '',
        };
    }

    async componentDidMount() {
        let upid = await fetchOwnUPID();

        this.setState({
            upid: upid,
        });
    }

    render() {
        let profilePic;

        if(this.state.upid.length > 0) {
            profilePic = (
                <img src={'/b/picture/' + this.state.upid} width="36px" height="36px" className="personal-info-pic" />
            );
        }

        return (
          <div className="personal-info">
              {profilePic}

              <div className="logout-pic" onClick={
                  async (e) => {
                      await revokeToken();
                  }
              }>
                  <img src={logoutPic} width="36px" height="36px" className="logout-pic-img" />
              </div>
              <input type="file" style={{display: 'none'}} ref={(c) => {this.fileInput = c}} onChange={
                  async (e) => {
                      if (this.fileInput.files.length > 0) {
                          await uploadProfilePicture(this.fileInput.files[0]);
                          location.reload();
                      }
                  }
              } />
              <div className="change-profile-pic" onClick={
                  (e) => {
                      this.fileInput.click();
                  }
              }>
                  <img src={personPic} width="36px" height="36px" className="change-profile-pic-img" />
              </div>
          </div>
        );
    }
}