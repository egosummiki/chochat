import React from 'react';
import logo from "../images/logo.png";
import '../styles/logout.css';

export default function LogoutComponent() {
    return (
        <div className='logout-message'>
            <div className="logo">
                <img src={logo} alt="cho-chat"/>
            </div>
            <div className='message'>
                <p>Zostałeś wylogowany</p>
                <p>możesz się <a href='/login'>zalogować</a> ponownie</p>
            </div>
        </div>
    );
}
