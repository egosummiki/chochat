import React from 'react';
import '../styles/register.css';
import logo from '../images/logo.png'
import queryString from "query-string";

const regEndpoint = '/b/register';

const InvalidChars = (
    <div className="incorrect-values">
        Wprowadzono nieprawidłowy znak
    </div>
);
const EmptyFields = (
    <div className="incorrect-values">
        Nie wszystkie pola zostały wypełnione
    </div>
);
const IncorrectEmail = (
    <div className="incorrect-values">
        Wprowadzono nieprawidłowy adres email
    </div>
);
const IncorrectPassword = (
    <div className="incorrect-values">
        Hasła się niepokrywają
    </div>
);
const IncorrectUnknown = (
    <div className="incorrect-values">
        Proszę spróbować później
    </div>
);


export default class RegisterForm extends React.Component {
    render() {
        let errorMsg;
        let errCode = queryString.parse(this.props.location.search).incorrect;
        switch (errCode) {
            case "0":
                errorMsg = InvalidChars;
                break;
            case "1":
                errorMsg = EmptyFields;
                break;
            case "2":
                errorMsg = IncorrectEmail;
                break;
            case "3":
                errorMsg = IncorrectPassword;
                break;
            case "4":
                errorMsg = IncorrectUnknown;
                break;
        }

        return (
            <div className="register-form">
                <div className="logo">
                    <img src={logo} alt="cho-chat"/>
                </div>
                <div className="new-account-header">
                    Załóż nowe konto
                </div>
                {errorMsg}
                <form method="POST" id="register-form-form" action={regEndpoint}>
                    <div className="row">
                        <label>
                            Imię <br/>
                            <input type="text" name="name"/>
                        </label>
                        <label>
                            Nazwisko <br/>
                            <input type="text" name="surname"/>
                        </label>
                    </div>
                    <div className="row">
                        <label>
                            Email <br/>
                            <input type="text" name="email"/>
                        </label>
                    </div>
                    <div className="row">
                        <label>
                            Hasło <br/>
                            <input type="password" name="password"/>
                        </label>
                        <label>
                            Powtórz <br/>
                            <input type="password" name="password_rep"/>
                        </label>
                    </div>

                    <div className="row">
                        <a className="login-instead" href="./login">Możesz też się zalogować</a>
                        <input type="submit" className="create-account" value="ZAŁÓŻ"/>
                    </div>

                </form>
            </div>
        );
    }
}
