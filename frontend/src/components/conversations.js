import React from 'react';
import '../styles/conversation.css'
import {fetchConversations} from "../conversations";
import {fetchContacts} from "../contacts";

function GetGroupChatName(members) {
    let names = members[0].name + ", " + members.slice(1).map(m => (" " + m.name)).join();
    if(names.length > 32) {
        names = names.substr(0, 30) + "...";
    }
    return names;
}
function ConversationBox(props) {
    if(props.messages == null || props.msgSubmit == null) {
        return (<div/>);
    }

    if(props.members === undefined || props.members.length === 0) {
        return (
            <div className="conversation">Tylko ja</div>
        )
    }

    if(props.members.length === 1) {
        return (
            <div className="conversation" onClick={() => {
                props.messages.setConversation(props.members[0].cid, props.members);
                props.msgSubmit.setCID(props.members[0].cid, props.messages);
                props.userInfo.setData(props.members[0].fullname, props.members[0].upid);
            }}>
                <div className="conv-profile-pic">
                    <img src={'/b/picture/' + props.members[0].upid} width="48px" height="48px" className="profile-pic"/>
                </div>
                <div className="conv-label">{props.members[0].fullname}</div>
            </div>
        )
    }

    let names = GetGroupChatName(props.members);

    return (<div className="conversation" onClick={() => {
        props.messages.setConversation(props.members[0].cid, props.members);
        props.msgSubmit.setCID(props.members[0].cid, props.messages);
        props.userInfo.setData(names, props.members[0].upid);
    }}>
        <div className="conv-profile-pic">
            <img src={'/b/picture/' + props.members[0].upid} width="48px" height="48px" className="profile-pic"/>
        </div>
        <div className="conv-label">{names}</div>

    </div>);
}

export class Conversations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            conversations: [],
        };

        this.messages = null;
        this.messageSubmit = null;
        this.userInfo = null;
        this.contactMap = {};
        this.conversations = [];
    }

    setReferences(msgs, msgSubmit, userInfo) {
        this.messages = msgs;
        this.messageSubmit = msgSubmit;
        this.userInfo = userInfo;
        this.setState({});
    }

    addContact(upid, contact) {
        this.contactMap[upid] = contact;
        for(let con in this.conversations) {
            if(this.conversations[con].length === 1) {
                if(upid === this.conversations[con][0].upid) {
                    this.conversations[con][0].fullname = contact.name + ' ' + contact.surname;
                    this.conversations[con][0].name = contact.name;
                    this.setState({conversations: this.conversations});
                    return;
                }
            }
        }
    }

    addNewConversation(cid, memberUpids) {
        this.addConversation(cid, memberUpids);
        this.setState({conversations: this.conversations});
    }

    addConversation(cid, memberUpids) {
        let members = [];
        for(let id in memberUpids) {
            let contact = this.contactMap[memberUpids[id]];
            if(contact === undefined) {
                members.push({fullname: "Nieznany Użytkownik", name: '?', upid: memberUpids[id], cid: cid});
                continue
            }
            members.push({fullname: contact.name + " " + contact.surname, name: contact.name, upid: memberUpids[id], cid: cid});
        }
        this.conversations.push(members);
    }

    async loadConversations() {
        let rawConvs = await fetchConversations();
        this.contactMap = await fetchContacts();

        this.conversations = [];
        for(let cid in rawConvs) {
            this.addConversation(cid, rawConvs[cid]);
        }
        this.setState({conversations: this.conversations});
    }

    async componentDidMount() {
        await this.loadConversations();
    }

    render() {
        return this.state.conversations.map(conv => (<ConversationBox members={conv} msgSubmit={this.messageSubmit} messages={this.messages} userInfo={this.userInfo}/>));
    }
}
