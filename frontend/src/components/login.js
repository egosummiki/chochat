import React from 'react';
import '../styles/login.css'
import logo from '../images/logo.png'
import queryString from 'query-string'

const authEndpoint = '/b/auth';

const infoIncorrect = (
    <div className="incorrect-credentials">
        Wprowadzono nieprowane dane
    </div>
);
const infoEmpty = (
    <div className="incorrect-credentials">
        Nie wszystkie pola zostały wypełnione
    </div>
);
const infoInvalidEmail = (
    <div className="incorrect-credentials">
        Podana nazwa użytkownika nie może być uznana za poprawny adres email
    </div>
);

function getInfo(form) {
    let errCode = queryString.parse(form).incorrect;
    switch (errCode) {
        case "1":
            return infoEmpty;
        case "2":
            return infoInvalidEmail;
        case "3":
            return infoIncorrect;
    }
    return null;
}

export default class LoginForm extends React.Component {

    render() {
        let info = getInfo(this.props.location.search);

        return (
            <div className="login-form">
                <div className="logo">
                    <img src={logo} alt="cho-chat"/>
                </div>
                {info}
                <form method="POST" id="login-form-form" action={authEndpoint}>
                    <input type="hidden" name="client_id" value="chochat" />

                    <div className="email-prompt">
                        <label>
                            Email
                            <input type="text" name="email"/>
                        </label>
                    </div>
                    <div className="password-prompt">
                        <label>
                            Hasło
                            <input type="password" name="password"/>
                        </label>
                    </div>

                    <div className="row">
                        <a className="new-account" href="./register">ZAŁÓŻ KONTO</a>
                        <input type="submit" value="ZALOGUJ ➜" className="login-submit"/>
                    </div>
                </form>
            </div>
        );
    }
}