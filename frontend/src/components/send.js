import React from "react";
import {postTyping, sendMessage} from "../messages";

export default class SendMessage extends React.Component {

    constructor(props) {
        super(props);
        this.cid = 0;
    }

    setCID(cid, messages) {
        this.cid = cid;
        this.messages = messages;
        this.postedTyping = false;
    }

    async sendMessage() {
        this.postedTyping = false;
        if(this.cid === 0) {
            return;
        }
        await sendMessage(this.cid, this.msgInput.value);
        this.messages.appendNewMessage('', this.msgInput.value);
        this.msgInput.value = '';
    }

    async onKeyPress(e) {
        if(e.key === 'Enter') {
            await this.sendMessage();
        } else if(!this.postedTyping) {
            this.postedTyping = true;
            await postTyping(this.cid);
        }
    };

    render() {
        return (
            <div className="new-message">
                <textarea name="message" id="message-input" ref={(c) => this.msgInput = c} onKeyPress={(e) => this.onKeyPress(e)}/>
                <div id="message-submit" onClick={this.sendMessage}>➤</div>
            </div>
        );
    }
}