import React from 'react';
import '../styles/chat.css';
import { Conversations } from "./conversations";
import Messages from "./messages";
import SendMessage from "./send";
import SearchBox from "./search_box";
import SearchResult from "./search_results";
import UserInfo from "./user_info";
import PersonalInfo from "./personal_info";
import { LoadToken, PostNotification, RefreshEndpoint, RefreshToken } from "../utils";
import { fetchContacts } from "../contacts";

const NewMessage = 1;
const Typing = 2;
const StoppedTyping = 3;
const NewContact = 4;
const NewConversation = 5;

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      leftPane: 'conv',
      searchTerm: '',
    };
    this.convs = null;
  }

  onWebSocketMessage(e) {
    let info = JSON.parse(e.data);
    console.log('WEBSOCKET:' + e.data);
    switch (info.type) {
      case NewMessage: // new message
        let from = this.contacts[info.data.from];
        PostNotification('Nowa wiadomość od ' + from.name + ' ' + from.surname + ': ' + info.data.contents);

        if (this.msgs.state.cid === info.data.cid) {
          this.msgs.appendNewMessage(info.data.from, info.data.contents);
        }
        break;
      case Typing:
        if (this.msgs.state.cid === info.data.cid) {
          this.msgs.setTyping(this.contacts[info.data.upid].name);
        }
        break;
      case NewContact:
        this.convs.addContact(info.data.upid, {
          name: info.data.name,
          surname: info.data.surname,
        });
        if (this.userInfo.state.upid === info.data.upid) {
          this.userInfo.setData(info.data.name + ' ' + info.data.surname, info.data.upid);
        }
        break;
      case NewConversation:
        this.convs.addNewConversation(info.data.cid, info.data.members);
    }
  };

  setupWebSocket() {
    let token = LoadToken();

    let url = new URL(window.location);
    url.protocol = 'ws:';
    url.pathname = '/b/ws';
    url.searchParams.set('token', token);

    this.socket = new WebSocket(url.toString());
    this.socket.onmessage = (e) => {
      this.onWebSocketMessage(e);
    };
    this.socket.onerror = (e) => {
      console.log('websocket error');
    };
    this.socket.onclose = (e) => {
      console.log('Resetting websocket connection');
      this.setupWebSocket();
    };
  }

  async componentDidMount() {
    this.convs.setReferences(this.msgs, this.msgSubmit, this.userInfo);
    this.setupWebSocket();
    this.contacts = await fetchContacts();
    setInterval(RefreshToken, 30000);
  }

  searchUsers(term) {
    this.setState((state, props) => ({
      leftPane: 'searchres',
      searchTerm: term,
    }));
  }

  viewConversations() {
    this.setState((state, props) => ({
      leftPane: 'conv',
      searchTerm: '',
    }));
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.leftPane === 'searchres') {
      await this.searchRes.searchTerm(this.state.searchTerm);
    } else {
      this.convs.setReferences(this.msgs, this.msgSubmit, this.userInfo);
      await this.convs.loadConversations();
    }
  }

  render() {
    let contactContents;
    if (this.state.leftPane === 'conv') {
      contactContents = (
        <Conversations ref={(c) => this.convs = c} messages={this.msgs} />
      );
    } else {
      contactContents = (
        <SearchResult ref={(c) => this.searchRes = c} messages={this.msgs} chat={this} userInfo={this.userInfo} messageSubmit={this.msgSubmit} />
      );
    }

    return (
      <div className="chat-window">
        <div className="left-pane">
          <PersonalInfo />
          <SearchBox chatComponent={this} ref={(c) => this.searchBox = c} />
          <div className="contacts">
            {contactContents}
          </div>
        </div>
        <div className="right-pane">
          <UserInfo ref={(c) => this.userInfo = c} />
          <Messages ref={(c) => this.msgs = c} />
          <SendMessage ref={(c) => this.msgSubmit = c} />
        </div>
      </div>
    );
  }
}
