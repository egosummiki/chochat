import * as React from "react";

export default class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            upid: '',
        };
    }

    setData(fullname, upid) {
        this.setState({
            fullname: fullname,
            upid: upid,
        });
    }

    render() {
        let profilePic;
        if(this.state.upid.length > 0) {
            profilePic = (
                <img src={'/b/picture/' + this.state.upid} width="48px" height="48px" className="profile-pic" />
            );
        }

        return (
            <div className="user-info">
                <div className="conv-profile-pic">
                    {profilePic}
                </div>
                <div className="conv-label">{this.state.fullname}</div>
            </div>
        );
    }
}