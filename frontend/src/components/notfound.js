import React from 'react';
import '../styles/notfound.css'

export default function NotFound() {
    return (
        <div className="not-found">
            <div className="status-label">
                404 NOT FOUND
            </div>
            Podana strona nie istnieje
        </div>
    )
}