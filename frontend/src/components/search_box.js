import React from 'react';
import loop from '../images/loop.png';

export default class SearchBox extends React.Component {
    onClick = (e) => {
        this.props.chatComponent.searchUsers(this.searchInput.value);
    };

    onKeyPress = (e) => {
        if(e.key === 'Enter') {
            this.props.chatComponent.searchUsers(this.searchInput.value);
        }
    };

    render() {
        return (
            <div className="search-box">
                <div className="search-submit" onClick={this.onClick}>
                    <img src={loop} width="32px" height="32px" />
                </div>
                <input type="text" id="search-input" ref={(c) => this.searchInput = c} onKeyPress={this.onKeyPress}/>
            </div>
        );
    }
}