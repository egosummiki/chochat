import React from 'react';
import {addYourselfToContacts, createConversation, fetchSearchContacts} from "../search";

function ContactBox(props) {
    return (
        <div className="conversation" onClick={async (e) => {
            await addYourselfToContacts(props.upid);
            let cid = await createConversation(props.upid);
            await props.messages.setConversation(cid, [
                {
                    fullname: props.name + ' ' + props.surname,
                    name: props.name,
                    upid: props.upid,
                },
            ]);
            props.userInfo.setData(props.name + ' ' + props.surname, props.upid);
            props.messageSubmit.setCID(cid, props.messages);
        }}>
            <div className="conv-profile-pic">
                <img src={'/b/picture/' + props.upid} width="48px" height="48px" className="profile-pic" />
            </div>
            <div className="conv-label">{props.name} {props.surname}</div>
        </div>
    );
}

export default class SearchResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            term: '',
            result: [],
        };
    }

    async searchTerm(term) {
        let result = await fetchSearchContacts(term);
        this.setState((state, props) => ({
            term: term,
            result: result === null ? [] : result,
        }));
    }

    render() {
        return (
            <div className="search-result">
                <div className="search-label">Wyniki wyszukiwania dla: <b>{this.state.term}</b></div>

                <div className="search-return" onClick={(e) => {
                    this.props.chat.viewConversations();
                    this.props.chat.searchBox.searchInput.value = '';
                }}>Wróć</div>

                {this.state.result.map(r => (<ContactBox upid={r.upid} name={r.name} surname={r.surname} messages={this.props.messages} userInfo={this.props.userInfo} messageSubmit={this.props.messageSubmit}/> ))}
            </div>
        );
    }
}