import {
    ConversationEndpoint,
    HasContactEndpoint,
    LoadToken,
    MessageEndpoint,
    MessageSendEndpoint, RevokeEndpoint, TypingEndpoint, UploadProfilePicEndpoint,
    WhoAmIEndpoint
} from "./utils";

export async function fetchMessages(cid) {
    let token = LoadToken();

    const response = await fetch(MessageEndpoint + cid, {
        method: 'get',
        headers: {
            'token': token
        },
    });
    switch(response.status) {
        case 200:
            break;
        case 401, 403:
            window.location = '/logout';
            break;
        default:
            alert("Nie można załadować wiadomości");
            break;
    }

     return response.json();
}

export async function sendMessage(cid, contents) {
    let token = LoadToken();

    const response = await fetch(MessageSendEndpoint, {
        method: 'post',
        headers: {
            'token': token,
        },
        body: JSON.stringify({
            cid: cid,
            contents: contents,
        }),
    });
    switch(response.status) {
        case 200, 201:
            break;
        case 401, 403:
            window.location = '/logout';
            break;
        default:
            alert("Nie można wysłać wiadomości");
            break;
    }
}

export async function postTyping(cid) {
    let token = LoadToken();

    let response = await fetch(TypingEndpoint + cid, {
        method: 'post',
        headers: {
            'token': token,
        }
    });
    if(response.status === 403 || response.status === 401) {
        window.location = '/logout';
    }
}

export async function fetchOwnUPID() {
    let token = LoadToken();

    let response = await fetch(WhoAmIEndpoint, {
        method: 'get',
        headers: {
            'token': token,
        }
    });
    switch(response.status) {
        case 200:
            break;
        case 401, 403:
            window.location = '/logout';
            break;
        default:
            alert("could fetch upid");
            break;
    }

    return response.text();
}

export async function uploadProfilePicture(file) {
    let token = LoadToken();

    let response = await fetch(UploadProfilePicEndpoint, {
        method: 'post',
        headers: {
            'token': token,
            'Content-Type': file.type,
        },
        body: file,
    });
    switch(response.status) {
        case 200:
            break;
        case 401, 403:
            window.location = '/logout';
            break;
        default:
            alert("Błąd w trakcie wysyłania zdjęcia");
            break;
    }

    return response.text();
}

export async function checkIfHasContact(upid) {
    let token = LoadToken();

    const response = await fetch(HasContactEndpoint + upid, {
        method: 'get',
        headers: {
            'token': token,
        }
    });
    switch(response.status) {
        case 200:
            break;
        case 401, 403:
            window.location = '/logout';
            break;
        default:
            alert("Nie można sprawdzić dostępności kontaktu");
            break;
    }

    return response.text();
}

export async function revokeToken() {
    let token = LoadToken();

    await fetch(RevokeEndpoint, {
        method: 'post',
        headers: {
            'token': token,
        }
    });

    window.location = '/logout';
}