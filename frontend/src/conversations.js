import {LoadToken, ConversationEndpoint} from './utils'

export async function fetchConversations() {
    let token = LoadToken();

    const response = await fetch(ConversationEndpoint, {
        method: 'get',
        headers: {
            'token': token,
        }
    });

    return await response.json();
}