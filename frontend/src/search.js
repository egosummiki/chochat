import {ContactEndpoint, ConversationEndpoint, LoadToken, SearchEndpoint} from "./utils";

export async function fetchSearchContacts(term) {
    let token = LoadToken();

    let result = await fetch(SearchEndpoint + encodeURIComponent(term), {
        method: 'get',
        headers: {
            'token': token,
        }
    });

    return result.json();
}

export async function addYourselfToContacts(upid) {
    let token = LoadToken();

    await fetch(ContactEndpoint, {
        method: 'post',
        headers: {
            'token': token,
        },
        body: upid,
    });
}

export async function createConversation(upid) {
    let token = LoadToken();

    let res = await fetch(ConversationEndpoint, {
        method: 'post',
        headers: {
            'token': token,
        },
        body: JSON.stringify([upid]),
    });

    return res.text();
}