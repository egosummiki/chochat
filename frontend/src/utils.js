export function getCookie(cname) {
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');

    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function LoadToken() {
    let token = getCookie('token');
    if(token === undefined || token === '') {
        window.location = '/logout';
        return;
    }
    return token;
}

export function PostNotification(contents) {
    if(Notification.permission === 'granted') {
        new Notification(contents);
    } else if(Notification.prototype !== 'denied') {
        Notification.requestPermission((permission => {
            if(permission === 'granted') {
                new Notification(contents);
            }
        }));
    }
}

export async function RefreshToken() {
    let token = LoadToken();

    await fetch(RefreshEndpoint, {
        method: 'post',
        headers: {
            'token': token,
        },
    });
}

export const ContactEndpoint = '/b/contact';
export const RefreshEndpoint = '/b/refresh';
export const TypingEndpoint = '/b/typing/';
export const UploadProfilePicEndpoint = '/b/picture';
export const HasContactEndpoint = '/b/hascontact/';
export const MessageEndpoint = '/b/msg/';
export const MessageSendEndpoint = '/b/msg';
export const WhoAmIEndpoint = '/b/whoami';
export const ConversationEndpoint = '/b/conversation';
export const SearchEndpoint = 'b/search/contact?q=';
export const RevokeEndpoint = 'b/revoke';
