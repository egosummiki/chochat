package main

import (
	"context"
	"fmt"
	"os"

	"github.com/vrischmann/envconfig"

	"chochat"
	"chochat/log"
	"chochat/service"
	"chochat/storage"
)

func main() {
	ctx, err := log.Inject(context.Background())
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "could not inject log: %s\n", err)
		os.Exit(1)
	}

	var conf chochat.Config
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(ctx, err).Error("could not read config")
		os.Exit(2)
	}

	store, err := storage.NewStorage(conf)
	if err != nil {
		log.WithError(ctx, err).Error("could not init storage")
		os.Exit(3)
	}

	chochatService, err := service.NewService(ctx, store, conf)
	if err != nil {
		log.WithError(ctx, err).Error("could not init service")
		os.Exit(4)
	}

	log.With(ctx, log.Prop("listen", conf.Listen)).Info("starting api")

	if err := service.ServeApi(ctx, conf, chochatService); err != nil {
		log.WithError(ctx, err, log.Prop("listen", conf.Listen)).Error("failed to start api")
	}
}
