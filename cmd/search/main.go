package main

import (
	"fmt"
	"log"

	"chochat"
	"chochat/search"
)

func main() {
	s, err := search.NewSearch("http://127.0.0.1:9200")
	if err != nil {
		log.Fatal(err)
	}

	var u1 chochat.UserID = 502888583636221953
	var u2 chochat.UserID = 502910830480818177

	h1, _ := u1.Hash()
	h2, _ := u2.Hash()

	fmt.Println(h1.String())
	fmt.Println(h2.String())

	contacts, err := s.SearchContacts("Stonoga")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%+v\n", contacts)
}
