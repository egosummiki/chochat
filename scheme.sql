CREATE TABLE users (
    id      SERIAL,
    email   VARCHAR(128) NOT NULL,
    name    VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
    secret  VARCHAR(128) NOT NULL,

    PRIMARY KEY (id),
    UNIQUE(email)
);

CREATE TABLE conv (
    id   SERIAL,
    upid VARCHAR(128) NOT NULL,

    PRIMARY KEY (id, upid)
);

CREATE TABLE msg (
    id       SERIAL,
    cid      SERIAL NOT NULL,
    sender   VARCHAR(128) NOT NULL, -- upid uuid
    contents VARCHAR(128),
    ts       TIMESTAMP,

    PRIMARY KEY(id)
);

CREATE TABLE contact (
    owner   VARCHAR(128) NOT NULL,
    contact SERIAL NOT NULL,

    PRIMARY KEY (owner, contact),
    FOREIGN KEY (contact) REFERENCES users(id)
);

CREATE TABLE initial (
    upid   VARCHAR(128) NOT NULL,
    symbol VARCHAR(1) NOT NULL,

    PRIMARY KEY (upid)
);

CREATE TABLE picture (
    upid    VARCHAR(128) NOT NULL,
    picture BYTEA NOT NULL,

    PRIMARY KEY(upid)
);
