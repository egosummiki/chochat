package socket

import "chochat"

type EventType int

const (
	EventNone EventType = iota
	EventNewMessage
	EventTyping
	EventStoppedTyping
	EventNewContact
	EventNewConversation
	EventNewUserInfo
)

type Event struct {
	Type EventType              `json:"type"`
	Data map[string]interface{} `json:"data"`
}

type EventDispatch struct {
	Event Event
	UPID  chochat.UserPublicID
}