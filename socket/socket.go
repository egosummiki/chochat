package socket

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"

	"chochat"
	"chochat/log"
)

const (
	expectedOrigin = "http://localhost"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Connection struct {
	Conn  *websocket.Conn
	Mutex *sync.Mutex
}

type Manager struct {
	Connections map[chochat.UserPublicID]Connection
}

func NewManager() *Manager {
	return &Manager{
		Connections: map[chochat.UserPublicID]Connection{},
	}
}

func (s *Manager) ServeWS(ctx context.Context, w http.ResponseWriter, r *http.Request, upid chochat.UserPublicID) error {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return fmt.Errorf("upgrader.Upgrade(): %w", err)
	}

	s.Connections[upid] = Connection{
		Conn:  conn,
		Mutex: &sync.Mutex{},
	}

	return nil
}

func (s *Manager) ListenForEvents(ctx context.Context, channel <-chan EventDispatch) {
	for {
		e, ok := <-channel
		if !ok {
			return
		}
		conn, ok := s.Connections[e.UPID]
		if !ok {
			continue
		}
		conn.Mutex.Lock()
		if err := handleEvent(ctx, conn.Conn, e.Event); err != nil {
			log.WithError(ctx, err).Info("could not handle event, closing connection")
			if err = conn.Conn.Close(); err != nil {
				log.WithError(ctx, err).Info("could not close connection")
			}
			delete(s.Connections, e.UPID) // broken connection remove
		}
		conn.Mutex.Unlock()
	}
}

func handleEvent(ctx context.Context, conn *websocket.Conn, e Event) error {
	if err := conn.SetWriteDeadline(time.Now().Add(time.Second * 10)); err != nil {
		return fmt.Errorf("conn.SetWriteDeadline(): %w", err)
	}

	writer, err := conn.NextWriter(websocket.TextMessage)
	if err != nil {
		return fmt.Errorf("conn.NextWriter(): %w", err)
	}
	defer func() {
		if err := writer.Close(); err != nil {
			log.WithError(ctx, err).Info("failed to close the writer")
		}
	}()

	if err := json.NewEncoder(writer).Encode(e); err != nil {
		return fmt.Errorf("json.NewEncoder().Encode(): %w", err)
	}

	log.With(ctx, log.Prop("event_type", int(e.Type))).Info("successfully sent event")
	return nil
}
