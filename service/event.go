package service

import (
	"chochat"
	"chochat/socket"
)

type option struct {
	key   string
	value interface{}
}

func withUPID(upid chochat.UserPublicID) option {
	return option{
		key:   "upid",
		value: upid.String(),
	}
}

func withFrom(upid chochat.UserPublicID) option {
	return option{
		key:   "from",
		value: upid.String(),
	}
}

func withName(name string) option {
	return option{
		key:   "name",
		value: name,
	}
}

func withSurname(surname string) option {
	return option{
		key:   "surname",
		value: surname,
	}
}

func withContents(contents string) option {
	return option{
		key:   "contents",
		value: contents,
	}
}

func withCID(cid chochat.ConvID) option {
	return option{
		key:   "cid",
		value:  cid.String(),
	}
}

func withMembers(upids []string) option {
	return option{
		key:   "members",
		value: upids,
	}
}

func (s *Service) postEvent(eventType socket.EventType, upid chochat.UserPublicID, values ...option) {
	valueMap := make(map[string]interface{})
	for _, pair := range values {
		valueMap[pair.key] = pair.value
	}

	s.eventChannel <- socket.EventDispatch{
		Event: socket.Event{
			Type: eventType,
			Data: valueMap,
		},
		UPID: upid,
	}
}
