package service

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"image/png"
	"io"
	"math/rand"
	"net/http"
	"time"

	"github.com/ory/fosite"

	"chochat"
	"chochat/auth"
	"chochat/image"
	"chochat/log"
	"chochat/search"
	"chochat/socket"
)

type Service struct {
	authProvider fosite.OAuth2Provider
	storage      chochat.Storage
	hasher       auth.Hasher
	sessions     map[chochat.UserID]auth.Session
	eventChannel chan socket.EventDispatch
	sockets      *socket.Manager
	search       *search.Search
}

func NewService(ctx context.Context, storage chochat.Storage, conf chochat.Config) (*Service, error) {
	authProvider, err := auth.NewAuth(conf)
	if err != nil {
		return nil, fmt.Errorf("auth.NewAuth(): %w", err)
	}

	s, err := search.NewSearch(conf.Elastic.Address)
	if err != nil {
		return nil, fmt.Errorf("search.NewSearch(): %w", err)
	}

	rand.Seed(time.Now().Unix())

	socketManager := socket.NewManager()
	eventChannel := make(chan socket.EventDispatch)

	go socketManager.ListenForEvents(ctx, eventChannel)

	return &Service{
		hasher:       auth.BCryptHasher{},
		authProvider: authProvider,
		storage:      storage,
		sessions:     map[chochat.UserID]auth.Session{},
		eventChannel: eventChannel,
		sockets:      socketManager,
		search:       s,
	}, nil
}

const SessionDuration = time.Hour

func (s *Service) generateSession(user chochat.User) (token chochat.AccessToken, expires time.Time) {
	secret := auth.GenerateSecret()
	expires = time.Now().Add(SessionDuration)
	s.sessions[user.ID] = auth.Session{
		Secret:  secret,
		Expires: expires,
	}
	token = auth.SignUser(user.Email, user.ID, secret)
	return
}

func (s *Service) VerifyUserCredentials(ctx context.Context, email string, password string) (token chochat.AccessToken, expires time.Time, err error) {
	user, err := s.storage.GetUserByEmail(ctx, email)
	if err != nil {
		log.WithError(ctx, err, log.Prop("email", email)).Info("could not find the user")
		return "", time.Unix(0, 0), fmt.Errorf("s.storage.GetUserByEmail(): %w", err)
	}

	err = s.hasher.Compare(password, user.Secret)
	if err != nil {
		log.WithError(
			ctx,
			err,
			log.Prop("email", email),
			log.Prop("uid", int(user.ID)),
		).Info("invalid credentials")
		return "", time.Unix(0, 0), fmt.Errorf("s.hasher.Compare(): %w", err)
	}

	token, expires = s.generateSession(user)
	return
}

func (s *Service) CreateUser(ctx context.Context, email, name, surname, password string) error {
	secret, err := s.hasher.Hash(password)
	if err != nil {
		return fmt.Errorf("hasher.Hash(): %w", err)
	}

	err = s.storage.CreateUser(ctx, chochat.User{
		Email:   email,
		Name:    name,
		Surname: surname,
		Secret:  secret,
	})
	if err != nil {
		return fmt.Errorf("s.storage.CreateUser(): %w", err)
	}

	user, err := s.storage.GetUserByEmail(ctx, email)
	if err != nil {
		return fmt.Errorf("s.storage.GetUserByEmail(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	err = s.search.IndexContact(chochat.Contact{
		Name:    name,
		Surname: surname,
		UPID:    upid.String(),
	})
	if err != nil {
		return fmt.Errorf("s.search.IndexContact(): %w", err)
	}

	return nil
}

func (s *Service) verifySession(ctx context.Context, token chochat.AccessToken) (chochat.User, error) {
	userName, hmacMac, err := auth.SplitToken(token)
	if err != nil {
		return chochat.User{}, fmt.Errorf("auth.SplitToken(): %w", err)
	}

	user, err := s.storage.GetUserByEmail(ctx, userName)
	if err != nil {
		return chochat.User{}, fmt.Errorf("s.storage.GetUserByEmail(): %w", err)
	}

	session, ok := s.sessions[user.ID]
	if !ok {
		return user, chochat.ErrSessionDoesntExists
	}

	if session.Expires.Before(time.Now()) {
		return user, chochat.ErrSessionExpired
	}

	if !auth.CheckSecret(user.ID, session.Secret, hmacMac) {
		return user, chochat.ErrInvalidSession
	}

	return user, nil
}

func (s *Service) VerifySession(ctx context.Context, token chochat.AccessToken) error {
	_, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}
	return nil
}

func (s *Service) RefreshUserToken(ctx context.Context, token chochat.AccessToken) (newToken chochat.AccessToken, expires time.Time, err error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return "", time.Unix(0, 0), fmt.Errorf("s.verifySession(): %w", err)
	}

	newToken, expires = s.generateSession(user)
	return
}

func (s *Service) RevokeToken(ctx context.Context, token chochat.AccessToken) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	delete(s.sessions, user.ID)
	return nil
}

func (s *Service) CreateConversation(ctx context.Context, token chochat.AccessToken, users []chochat.UserPublicID) (chochat.ConvID, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return 0, fmt.Errorf("s.verifySession(): %w", err)
	}

	puid, err := user.ID.Hash()
	if err != nil {
		return 0, fmt.Errorf("user.ID.Hash(): %w", err)
	}

	if len(users) == 1 {
		cid, err := s.storage.CheckIfConversationExists(ctx, puid, users[0])
		if err != nil {
			return 0, fmt.Errorf("s.storage.CheckIfConversationExists(): %w", err)
		}

		if cid != 0 {
			return cid, chochat.ErrConversationAlreadyExists
		}
	}

	corespondents := append([]chochat.UserPublicID{puid}, users...)
	cid, err := s.storage.CreateConversation(ctx, corespondents)
	if err != nil {
		return 0, fmt.Errorf("s.storage.CreateConversation(): %w", err)
	}

	for _, u := range users {
		var members []string
		for _, c := range corespondents {
			if !c.Equals(u) {
				members = append(members, c.String())
			}
		}
		s.postEvent(socket.EventNewConversation, u, withCID(cid), withMembers(members))
	}

	return cid, nil
}

func (s *Service) Message(ctx context.Context, token chochat.AccessToken, conv chochat.ConvID, contents string) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	err = s.storage.StoreMessage(ctx, conv, upid, contents)
	if err != nil {
		return fmt.Errorf("s.storage.StoreMessage(): %w", err)
	}

	members, err := s.storage.ListMembers(ctx, conv)
	if err != nil {
		return fmt.Errorf("s.storage.ListMembers(): %w", err)
	}

	for _, m := range members {
		if m != upid {
			s.postEvent(socket.EventNewMessage, m, withCID(conv), withFrom(upid), withContents(contents))
		}
	}

	return nil
}

func (s *Service) AddContact(ctx context.Context, token chochat.AccessToken, owner chochat.UserPublicID) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	err = s.storage.AddContact(ctx, user.ID, owner)
	if err != nil {
		return fmt.Errorf("s.storage.AddContact(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	s.postEvent(socket.EventNewContact, owner, withUPID(upid), withName(user.Name), withSurname(user.Surname))

	return nil
}

func (s *Service) ListContacts(ctx context.Context, token chochat.AccessToken) ([]chochat.Contact, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return nil, fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return nil, fmt.Errorf("user.ID.Hash(): %w", err)
	}

	contacts, err := s.storage.ListContacts(ctx, upid)
	if err != nil {
		return nil, fmt.Errorf("s.storage.ListContacts(): %w", err)
	}

	return contacts, nil
}

func (s *Service) ListConversations(ctx context.Context, token chochat.AccessToken) (chochat.ConvMap, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return nil, fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return nil, fmt.Errorf("user.ID.Hash(): %w", err)
	}

	convs, err := s.storage.ListConversations(ctx, upid)
	if err != nil {
		return nil, fmt.Errorf("s.storage.ListConversations(): %w", err)
	}

	return convs, nil
}

func (s *Service) ListMessages(ctx context.Context, token chochat.AccessToken, cid chochat.ConvID) ([]chochat.Message, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return nil, fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return nil, fmt.Errorf("user.ID.Hash(): %w", err)
	}

	msgs, err := s.storage.ListMessages(ctx, cid, upid.String())
	if err != nil {
		return nil, fmt.Errorf("s.storage.ListMessages(): %w", err)
	}

	return msgs, nil
}

func (s *Service) GetUserPublicID(ctx context.Context, token chochat.AccessToken) (chochat.UserPublicID, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return chochat.UserPublicID{}, fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return chochat.UserPublicID{}, fmt.Errorf("user.ID.Hash(): %w", err)
	}

	return upid, nil
}

func (s *Service) getFallbackProfilePicture(ctx context.Context, upid chochat.UserPublicID) ([]byte, error) {
	initial, err := s.storage.GetInitial(ctx, upid)
	if err != nil {
		return nil, fmt.Errorf("s.storage.GetInitial(): %w", err)
	}
	img := image.NewSymbol(initial)

	var buff bytes.Buffer
	if err := png.Encode(&buff, img); err != nil {
		return nil, fmt.Errorf("png.Encode(): %w", err)
	}

	return buff.Bytes(), nil
}

func (s *Service) GetUserProfilePicture(ctx context.Context, upid chochat.UserPublicID) ([]byte, error) {
	picBytes, err := s.storage.GetPicture(ctx, upid)
	switch {
	case err == nil:
	case errors.Is(err, chochat.ErrPictureNotFound):
		return s.getFallbackProfilePicture(ctx, upid)
	default:
		return nil, fmt.Errorf("s.storage.GetPicture(): %w", err)
	}

	return picBytes, nil
}

func (s *Service) SearchContacts(ctx context.Context, token chochat.AccessToken, term string) ([]chochat.Contact, error) {
	_, err := s.verifySession(ctx, token)
	if err != nil {
		return nil, fmt.Errorf("s.verifySession(): %w", err)
	}

	result, err := s.search.SearchContacts(term)
	if err != nil {
		return nil, fmt.Errorf("s.search.SearchContacts(): %w", err)
	}

	return result, nil
}

func (s *Service) HasContact(ctx context.Context, token chochat.AccessToken, upid chochat.UserPublicID) (bool, error) {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return false, fmt.Errorf("s.verifySession(): %w", err)
	}

	has, err := s.storage.HasContact(ctx, user.ID, upid)
	if err != nil {
		return false, fmt.Errorf("s.storage.HasContact(): %w", err)
	}

	return has, nil
}

func (s *Service) UploadProfilePicture(ctx context.Context, token chochat.AccessToken, mime string, data io.Reader) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	img, err := image.TransformPicture(mime, data)
	if err != nil {
		return fmt.Errorf("image.TransformPicture(): %w", err)
	}

	var buff bytes.Buffer
	if err := png.Encode(&buff, img); err != nil {
		return fmt.Errorf("png.Encode(): %w", err)
	}

	if err := s.storage.UploadPicture(ctx, upid, buff.Bytes()); err != nil {
		return fmt.Errorf("s.storage.UploadPicture(): %w", err)
	}

	return nil
}

func (s *Service) ServeWebSocket(ctx context.Context, writer http.ResponseWriter, request *http.Request, token chochat.AccessToken) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	_ = s.sockets.ServeWS(ctx, writer, request, upid)
	return nil
}


func (s *Service) PostTyping(ctx context.Context, token chochat.AccessToken, cid chochat.ConvID) error {
	user, err := s.verifySession(ctx, token)
	if err != nil {
		return fmt.Errorf("s.verifySession(): %w", err)
	}

	upid, err := user.ID.Hash()
	if err != nil {
		return fmt.Errorf("user.ID.Hash(): %w", err)
	}

	members, err := s.storage.ListMembers(ctx, cid)
	if err != nil {
		return fmt.Errorf("s.storage.ListMembers(): %w", err)
	}

	for _, m := range members {
		if m != upid {
			s.postEvent(socket.EventTyping, m, withUPID(upid), withCID(cid))
		}
	}

	return nil
}
