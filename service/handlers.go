package service

import (
	"context"
	"encoding/json"
	"errors"
	"html"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/google/uuid"

	"chochat"
	"chochat/log"
)

const (
	AuthErrorUnknown = iota
	AuthErrorEmptyFields
	AuthErrorIncorrectEmail
	AuthErrorInvalidCredentials
)

func handleAuth(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "auth")

	err := request.ParseForm()
	if err != nil {
		log.WithError(ctx, err, ep).Error("could not parse request")
		redirectError(writer, AuthErrorUnknown)
		return
	}

	email := html.EscapeString(request.FormValue("email"))
	password := html.EscapeString(request.FormValue("password"))
	if eitherIsEmpty(email, password) {
		log.With(ctx, ep).Info("empty fields")
		redirectError(writer, AuthErrorEmptyFields)
		return
	}

	if !emailRegexp.MatchString(email) {
		log.With(ctx, ep).Info("incorrect email address")
		redirectError(writer, AuthErrorIncorrectEmail)
		return
	}

	token, expires, err := service.VerifyUserCredentials(ctx, email, password)
	if err != nil {
		redirectError(writer, AuthErrorInvalidCredentials)
		return
	}

	http.SetCookie(writer, &http.Cookie{
		Name:    "token",
		Value:   string(token),
		Path:    "/",
		Expires: expires,
		Raw:     "overwrite=true",
	})

	writer.Header().Set("Location", redirectLoggedIn)
	writer.WriteHeader(http.StatusFound)
}

func handleRegister(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "register")

	err := request.ParseForm()
	if err != nil {
		log.WithError(ctx, err, ep).Error("could not parse request")
		redirectErrorReg(writer, 0)
		return
	}

	email := html.EscapeString(request.FormValue("email"))
	name := html.EscapeString(request.FormValue("name"))
	surname := html.EscapeString(request.FormValue("surname"))
	password := html.EscapeString(request.FormValue("password"))
	passwordRep := html.EscapeString(request.FormValue("password_rep"))

	if eitherIsEmpty(email, name, surname, password, passwordRep) {
		log.With(ctx, ep).Error("empty fields")
		redirectErrorReg(writer, 1)
		return
	}

	if !emailRegexp.MatchString(email) {
		log.With(ctx, ep).Error("incorrect email address")
		redirectErrorReg(writer, 2)
		return
	}
	if passwordRep != password {
		log.With(ctx, ep).Error("passwords to not match")
		redirectErrorReg(writer, 3)
		return
	}

	err = service.CreateUser(ctx, email, name, surname, password)
	if err != nil {
		log.WithError(ctx, err, ep).Error("could not create user")
		redirectErrorReg(writer, 4)
	}

	writer.Header().Set("Location", redirectLogin)
	writer.WriteHeader(http.StatusFound)
}

func handleVerify(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "refresh")

	at := request.Header.Get("token")
	if len(at) == 0 {
		log.With(ctx, ep).Info("verify hit without token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	err := service.VerifySession(ctx, chochat.AccessToken(at))
	if err != nil {
		log.WithError(ctx, err, ep).Info("could not verify session")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}
}

func handleRefresh(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "refresh")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep).Info("refresh hit without token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	newToken, expires, err := service.RefreshUserToken(ctx, chochat.AccessToken(token))
	if err != nil {
		log.WithError(ctx, err, ep).Info("could not verify session")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	http.SetCookie(writer, &http.Cookie{
		Name:    "token",
		Value:   string(newToken),
		Path:    "/",
		Expires: expires,
		Raw:     "overwrite=true",
	})
}

func handleRevoke(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "revoke")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep).Info("revoke hit without token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	if err := service.RevokeToken(ctx, chochat.AccessToken(token)); err != nil {
		log.WithError(ctx, err, ep).Info("could not revoke token")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}
}

func handleConversation(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "conversation")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep).Info("revoke hit without token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	defer request.Body.Close()

	var usersUUIDs []string
	err := json.NewDecoder(request.Body).Decode(&usersUUIDs)
	if err != nil {
		log.WithError(ctx, err, ep).Info("error decoding json")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	var users []chochat.UserPublicID
	for _, id := range usersUUIDs {
		upid, err := uuid.Parse(id)
		if err != nil {
			log.WithError(ctx, err, ep).Info("error parsing uuid")
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		users = append(users, chochat.UserPublicID(upid))
	}

	cid, err := service.CreateConversation(ctx, chochat.AccessToken(token), users)
	switch {
	case err == nil:
	case errors.Is(err, chochat.ErrConversationAlreadyExists):
		writer.WriteHeader(http.StatusAlreadyReported)
		_, _ = writer.Write([]byte(strconv.Itoa(int(cid))))
		return
	default:
		log.WithError(ctx, err, ep).Info("could not create conversation")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	_, err = writer.Write([]byte(strconv.Itoa(int(cid))))
	if err != nil {
		log.WithError(ctx, err, ep).Error("could not write request")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func handleListConversations(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "conversation")
	md := log.Prop("method", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	convs, err := service.ListConversations(ctx, chochat.AccessToken(token))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not list conversations")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	if err = json.NewEncoder(writer).Encode(convs); err != nil {
		log.WithError(ctx, err, ep, md).Error("could not encode convs")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
}

func handleMessage(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "msg")
	md := log.Prop("endpoint", "post")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty token")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	var msg struct {
		Conv     string `json:"cid"`
		Contents string `json:"contents"`
	}
	defer request.Body.Close()
	err := json.NewDecoder(request.Body).Decode(&msg)
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not decode message")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	cid, err := strconv.Atoi(msg.Conv)
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not decode message")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	err = service.Message(ctx, chochat.AccessToken(token), chochat.ConvID(cid), msg.Contents)
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("error sending message")
		writer.WriteHeader(chochat.GetStatusCode(err))
	}

	writer.WriteHeader(http.StatusCreated)
	return
}

func handleListMessages(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "msg")
	md := log.Prop("endpoint", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty token")
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	cid, err := strconv.Atoi(chi.URLParam(request, "cid"))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("invalid cid")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	msgs, err := service.ListMessages(ctx, chochat.AccessToken(token), chochat.ConvID(cid))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not list messages")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	if err = json.NewEncoder(writer).Encode(msgs); err != nil {
		log.WithError(ctx, err, ep, md).Error("could not encode")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
}

func handleContact(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "contact")
	md := log.Prop("method", "post")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	defer request.Body.Close()
	owner, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.WithError(ctx, err, ep, md).Error("could not read request body")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	upid, err := uuid.Parse(string(owner))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not parse upid")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	err = service.AddContact(ctx, chochat.AccessToken(token), chochat.UserPublicID(upid))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not add contact")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	writer.WriteHeader(http.StatusCreated)
}

func handleListContacts(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "contact")
	md := log.Prop("method", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	contacts, err := service.ListContacts(ctx, chochat.AccessToken(token))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not list contacts")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	if err = json.NewEncoder(writer).Encode(contacts); err != nil {
		log.WithError(ctx, err, ep, md).Error("could not encode contacts")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
}

func handleWhoAmiI(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "whoami")
	md := log.Prop("method", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	upid, err := service.GetUserPublicID(ctx, chochat.AccessToken(token))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not get upid")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	_, _ = writer.Write([]byte(upid.String()))
}

func handlePicture(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "pictures")
	md := log.Prop("method", "get")

	upid, err := chochat.UPIDFromString(chi.URLParam(request, "upid"))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not decide upid")
		writer.WriteHeader(http.StatusNotFound)
		return
	}

	imgData, err := service.GetUserProfilePicture(ctx, upid)
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not generate image")
		writer.WriteHeader(chochat.GetStatusCode(err))
		return
	}

	writer.Header().Set("Content-Type", "image/png")
	if _, err := writer.Write(imgData); err != nil {
		log.WithError(ctx, err, ep).Error("could not write request")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
}

const PictureUploadLimit = 1024*1024*60

func handleUploadPicture(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "picture")
	md := log.Prop("method", "post")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	mime := request.Header.Get("Content-Type")
	if len(mime) == 0 {
		log.With(ctx, ep, md).Info("empty mime type")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	maxReader := http.MaxBytesReader(writer, request.Body, PictureUploadLimit) // 60MB

	defer request.Body.Close()
	if err := service.UploadProfilePicture(ctx, chochat.AccessToken(token), mime, maxReader); err != nil {
		log.WithError(ctx, err, ep, md).Info("could not upload image")

		switch {
		case chochat.IsSessionError(err):
			writer.WriteHeader(http.StatusForbidden)
		case errors.Is(err, chochat.ErrUnknownMIME):
			writer.WriteHeader(http.StatusExpectationFailed)
		case errors.Is(err, chochat.ErrInvalidImage):
			writer.WriteHeader(http.StatusNotAcceptable)
		default:
			writer.WriteHeader(http.StatusInternalServerError)
		}

		return
	}
}

func handleSearchContacts(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "contact")
	md := log.Prop("method", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	if err := request.ParseForm(); err != nil {
		log.With(ctx, ep, md).Info("could not parse form")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	queryTerm := request.Form.Get("q")
	if len(queryTerm) == 0 {
		log.With(ctx, ep, md).Info("empty query term")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	result, err := service.SearchContacts(ctx, chochat.AccessToken(token), queryTerm)
	if err != nil {
		log.With(ctx, ep, md).Info("could not search contacts")
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	if err = json.NewEncoder(writer).Encode(result); err != nil {
		log.WithError(ctx, err, ep, md).Error("could not encode contacts")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	writer.Header().Set("Content-Type", "application/json")
}

func handleHasContact(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "hascontact")
	md := log.Prop("method", "get")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	upid, err := chochat.UPIDFromString(chi.URLParam(request, "upid"))
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not decide upid")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	result, err := service.HasContact(ctx, chochat.AccessToken(token), upid)
	if err != nil {
		log.WithError(ctx, err, ep, md).Info("could not check contact")
		writer.WriteHeader(http.StatusForbidden)
		return
	}

	if result {
		_, err = writer.Write([]byte("true"))
	} else {
		_, err = writer.Write([]byte("false"))
	}
	if err != nil {
		log.WithError(ctx, err, ep).Error("could not write request")
		writer.WriteHeader(http.StatusInternalServerError)
	}
}

func handleWebSocket(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	if err := request.ParseForm(); err != nil {
		log.WithError(ctx, err).Info("could not parse form")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	token := request.Form.Get("token")

	err := service.ServeWebSocket(ctx, writer, request, chochat.AccessToken(token))
	if err != nil {
		log.WithError(ctx, err).Info("could not create channel")
		writer.WriteHeader(http.StatusForbidden)
		return
	}
}

func handleTyping(ctx context.Context, writer http.ResponseWriter, request *http.Request, service chochat.Service) {
	ep := log.Prop("endpoint", "typing")
	md := log.Prop("method", "post")

	token := request.Header.Get("token")
	if len(token) == 0 {
		log.With(ctx, ep, md).Info("empty access token")
		writer.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	cid, err := strconv.Atoi(chi.URLParam(request, "cid"))
	if err != nil {
		log.WithError(ctx, err).Info("could not read upid")
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	err = service.PostTyping(ctx, chochat.AccessToken(token), chochat.ConvID(cid))
	if err != nil {
		log.WithError(ctx, err).Info("could not post typing")
		writer.WriteHeader(http.StatusForbidden)
		return
	}
}
