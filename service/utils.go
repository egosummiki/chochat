package service

func eitherIsEmpty(values ...string) bool {
	for _, v := range values {
		if len(v) == 0 {
			return true
		}
	}
	return false
}
