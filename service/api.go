package service

import (
	"context"
	"fmt"
	"net/http"
	"regexp"

	"github.com/go-chi/chi"

	"chochat"
)

var (
	emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	redirectLoggedIn   = "/chat"
	redirectFailure    = "/login?incorrect="
	redirectFailureReg = "/register?incorrect="
	redirectLogin      = "/login"
)

func ServeApi(ctx context.Context, conf chochat.Config, service chochat.Service) error {
	r := chi.NewRouter()

	r.Route("/b", func(r chi.Router) {
		r.Post("/auth", func(writer http.ResponseWriter, request *http.Request) {
			handleAuth(ctx, writer, request, service)
		})

		r.Post("/register", func(writer http.ResponseWriter, request *http.Request) {
			handleRegister(ctx, writer, request, service)
		})

		r.Get("/verify", func(writer http.ResponseWriter, request *http.Request) {
			handleVerify(ctx, writer, request, service)
		})

		r.Post("/refresh", func(writer http.ResponseWriter, request *http.Request) {
			handleRefresh(ctx, writer, request, service)
		})

		r.Post("/revoke", func(writer http.ResponseWriter, request *http.Request) {
			handleRevoke(ctx, writer, request, service)
		})

		r.Post("/conversation", func(writer http.ResponseWriter, request *http.Request) {
			handleConversation(ctx, writer, request, service)
		})

		r.Get("/conversation", func(writer http.ResponseWriter, request *http.Request) {
			handleListConversations(ctx, writer, request, service)
		})

		r.Post("/msg", func(writer http.ResponseWriter, request *http.Request) {
			handleMessage(ctx, writer, request, service)
		})

		r.Get("/msg/{cid}", func(writer http.ResponseWriter, request *http.Request) {
			handleListMessages(ctx, writer, request, service)
		})

		r.Post("/contact", func(writer http.ResponseWriter, request *http.Request) {
			handleContact(ctx, writer, request, service)
		})

		r.Get("/contact", func(writer http.ResponseWriter, request *http.Request) {
			handleListContacts(ctx, writer, request, service)
		})

		r.Get("/search/contact", func(writer http.ResponseWriter, request *http.Request) {
			handleSearchContacts(ctx, writer, request, service)
		})

		r.Get("/hascontact/{upid}", func(writer http.ResponseWriter, request *http.Request) {
			handleHasContact(ctx, writer, request, service)
		})

		r.Get("/whoami", func(writer http.ResponseWriter, request *http.Request) {
			handleWhoAmiI(ctx, writer, request, service)
		})

		r.Get("/picture/{upid}", func(writer http.ResponseWriter, request *http.Request) {
			handlePicture(ctx, writer, request, service)
		})

		r.Post("/picture", func(writer http.ResponseWriter, request *http.Request) {
			handleUploadPicture(ctx, writer, request, service)
		})

		r.Get("/ws", func(writer http.ResponseWriter, request *http.Request) {
			handleWebSocket(ctx, writer, request, service)
		})

		r.Post("/typing/{cid}", func(writer http.ResponseWriter, request *http.Request) {
			handleTyping(ctx, writer, request, service)
		})
	})

	if err := http.ListenAndServe(conf.Listen, r); err != nil {
		return fmt.Errorf("http.ListenAndServe(): %w", err)
	}
	return nil
}

func redirectError(writer http.ResponseWriter, errNum int) {
	writer.Header().Set("Location", fmt.Sprintf("%s%d", redirectFailure, errNum))
	writer.WriteHeader(http.StatusSeeOther)
}

func redirectErrorReg(writer http.ResponseWriter, errNum int) {
	writer.Header().Set("Location", fmt.Sprintf("%s%d", redirectFailureReg, errNum))
	writer.WriteHeader(http.StatusSeeOther)
}
